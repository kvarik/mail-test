﻿using MailAutomation.Utilities;
using MailAutomation.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using log4net.Config;
using System.Configuration;
using NUnit.Framework;
using MailAutomation.Pages.Mail;


namespace MailAutomation
{
      
    class Program
    {
        
        static void Main(string[] args)
        {

            IMail YandexMail = MailFactory.GetMailService("Yandex");

            DoLogin(YandexMail);

            string sentEmailSubj = SendRandomEmail(YandexMail);

            Assert.True(YandexMail.InboxPage.CheckEmailByTitle(sentEmailSubj));

            YandexMail.InboxPage.Exit();

        }

        private static string SendRandomEmail(IMail YandexMail)
        {
            YandexMail.InboxPage.NewEmail();

            string sentEmailSubj = YandexMail.NewEmailPage.SendRandomEmail();

            YandexMail.InboxPage.ClickInbox();
            return sentEmailSubj;
        }

        private static void DoLogin(IMail mailService)
        {
            mailService.LoginPage.Navigate();
            string login = MailAutomation.Properties.Settings.Default.Login;
            string password = MailAutomation.Properties.Settings.Default.Password;
            mailService.LoginPage.LogIn(login, password);
            Assert.True(mailService.InboxPage.CheckThatWeAreInside(), "Was unable to Log in mail service.");
        }
    }
}