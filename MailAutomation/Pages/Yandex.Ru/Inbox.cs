﻿using MailAutomation.Utilities;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using MailAutomation.Pages.Mail;

namespace MailAutomation.Pages.Mail.Yandex.Ru
{
    // Inside of mail service class.

    public class Inbox : BaseInbox
    {
        public Inbox(Driver d) : base(d) 
        { 
            errorMessageClassName = "error-msg";
            inboxLink = driver.WaitForElement(By.LinkText("Входящие"),15);
            toolbar = driver.WaitForElements(By.ClassName("b-toolbar__but"),15);
            newMailToolBar = "Написать";
            refreshToolBar = "Проверить";
            emailLinks = driver.FindElements(By.ClassName("b-messages__subject"));
            exitLinkText = "Выход";
        }

        public override string Url
        {
            get { return driver.Url; }
        }

        private static IWebElement LiteVersionLink
        {
            get
            {
                return driver.WaitForElement(By.LinkText("Лёгкая версия"), 15);
            }
        }

        public override bool CheckThatWeAreInside()
        {

            Logger.Log.Debug("Checking there are no errors.");
            if (ErrorMessageExists)
            {
                string errorMessage = ErrorMessage.Text;
                Logger.Log.Error("Error logging in: " + errorMessage);
                return false;
            }
            else
            {
                Logger.Log.Info("Yep! We're inside!");
                ClickLiteVersionLink();
                return true;
            }
        }

        private void ClickLiteVersionLink()
        {
            if (driver.CheckElementExists(By.LinkText("Лёгкая версия")))
            {
                LiteVersionLink.Click();
            }
        }

    }
}
