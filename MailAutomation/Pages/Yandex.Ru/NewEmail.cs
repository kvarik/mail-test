﻿using MailAutomation.Utilities;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace MailAutomation.Pages.Mail.Yandex.Ru
{
    public class NewEmail : BaseNewEmail
    {
        public NewEmail(Driver d) : base(d)
        {
            ToField = driver.FindElement(By.Name("to"));
            SubjField = driver.FindElement(By.Name("subj"));
            MailBody = driver.FindElement(By.Name("send"));
            SendButton = driver.FindElement(By.Name("doit"));
        }

        public override string Url
        {
            get { return driver.Url; }
        }

    }
}
