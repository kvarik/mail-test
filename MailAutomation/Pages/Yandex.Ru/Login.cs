﻿using MailAutomation.Utilities;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MailAutomation.Pages.Mail;

namespace MailAutomation.Pages.Mail.Yandex.Ru
{
    public class Login : BaseLogin
    {
        private string url = "http://mail.yandex.ru/";


        public override string Url
        {
            get { return url; }
        }
        
        public Login(Driver d)
            : base(d)
        {
            LoginEdit = driver.FindElement(By.Name("login"));
            PasswordEdit = driver.FindElement(By.Name("passwd"));
        }

    }
}
