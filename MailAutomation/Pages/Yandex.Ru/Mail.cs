﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MailAutomation.Pages;
using OpenQA.Selenium;
using MailAutomation.Utilities;
using MailAutomation;
using System.Collections.ObjectModel;
using MailAutomation.Pages.Mail;



namespace MailAutomation.Pages.Mail.Yandex.Ru
{

    public class Mail : IMail
    {
        public static Driver driver;
        public static BaseLogin loginPage;
        public static BaseNewEmail newEmailPage;
        public static BaseInbox inboxPage;

        public Mail(Driver d)
        {
            driver = d;
            }

        Driver IMail.driver
        {
            get
            {
                return driver;
            }
            set
            {
                driver = value;
            }
        }

        BaseLogin IMail.LoginPage
        {
            get
            {
                return new Login(driver);
            }
        }

        BaseInbox IMail.InboxPage
        {
            get
            {
                return new Inbox(driver);
            }
        }
        
        BaseNewEmail IMail.NewEmailPage
        {
            get
            {
                return new NewEmail(driver);
                ;
            }
        }

 
    }
}