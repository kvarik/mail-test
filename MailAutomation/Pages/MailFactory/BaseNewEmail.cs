﻿using MailAutomation.Utilities;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MailAutomation.Pages.Mail
{
    public abstract class BaseNewEmail : BasePage
    {
        public BaseNewEmail(Driver d)
            : base(d)
        {

        }
        public static IWebElement ToField;
        public static IWebElement SubjField;
        public static IWebElement MailBody;
        public static IWebElement SendButton;
        public static IWebElement EmailSentMessage;

        private void PopulateTo(string to)
        {
            try
            {
                Logger.Log.Debug("Populating field 'To': " + to);
                ToField.SendKeys(to);
                Logger.Log.Debug(ToField.ToString() + " is populated to: " + to);
            }
            catch (Exception e)
            {
                Logger.Log.Fatal("Couldn't populate field 'To'", e);
            }
        }

        private string PopulateSubj()
        {
            string subj = "Test ";
            subj += MD5Util.GetMd5Hash(DateTime.Now.ToString());
            try
            {
                Logger.Log.Debug("Populating subject field with: " + subj);
                SubjField.Click();
                SubjField.SendKeys(subj);
                return subj;
            }
            catch (Exception e)
            {
                Logger.Log.Error("Couldn't populate subject field with: " + subj);
                return null;
            }

        }

        private void PopulateBody(string body)
        {
            try
            {
                Logger.Log.Debug("Populating mail body with: " + body);
                MailBody.Click();
                MailBody.SendKeys(body);
                Logger.Log.Debug("Mail body populated with: " + body);
            }
            catch (Exception e)
            {
                Logger.Log.Error("Couldn't populate mail body.", e);
            }
        }

        private void ClickSend()
        {
            try
            {
                Logger.Log.Debug("Clicking 'Send' button");
                SendButton.Click();
                Logger.Log.Debug("Button 'Send' clicked");
            }
            catch (Exception e)
            {
                Logger.Log.Error("Button 'Send' not found");
            }
        }

        // Returns subject of an email for further checking

        public string SendRandomEmail()
        {
            //TODO: Domain to config
            PopulateTo(Properties.Settings.Default.Login + "@yandex.ru");
            string subj = PopulateSubj();

            PopulateBody(subj);
            ClickSend();
            return subj;
        }


    }
}
