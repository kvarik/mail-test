﻿using System;
using MailAutomation.Pages;
using MailAutomation.Utilities;

namespace MailAutomation.Pages.Mail
{
    public interface IMail
    {
        Driver driver { get; set; }
        BaseLogin LoginPage { get;  }
        BaseNewEmail NewEmailPage { get;  }
        BaseInbox InboxPage { get;  }
    }
}
