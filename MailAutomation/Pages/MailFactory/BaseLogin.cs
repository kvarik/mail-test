﻿using MailAutomation.Utilities;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MailAutomation.Pages.Mail
{
    public abstract class BaseLogin : BasePage
    {
        public BaseLogin(Driver d)
            : base(d)
        { }

        public static IWebElement LoginEdit;

        public static IWebElement PasswordEdit;

        public void LogIn(string login, string password)
        {
            Logger.Log.Info("Logging in to mail service.");
            SetLogin(login);
            SetPassword(password);
            ClickLogin();
        }


        private static void SetLogin(string loginName)
        {
            try
            {
                LoginEdit.Clear();
                LoginEdit.SendKeys(loginName);
                Logger.Log.Debug("Login is set to: " + loginName);
            }
            catch (Exception e)
            {
                Logger.Log.Fatal("Couldn't populate login.", e);
            }
        }
        private static void SetPassword(string password)
        {
            try
            {
                PasswordEdit.SendKeys(password);
                Logger.Log.Debug("Password is set.");
            }
            catch (Exception e)
            {
                Logger.Log.Fatal("Couldn't populate password.", e);
            }
        }

        private static void ClickLogin()
        {
            try
            {
                Logger.Log.Debug("Submitting login form.");
                PasswordEdit.Submit();
            }
            catch (Exception e)
            {
                Logger.Log.Fatal("Couldn't submit login form.", e);
            }
        }
    }
}
