﻿using MailAutomation.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MailAutomation.Pages.Mail
{
    public static class MailFactory
    {
        public static IMail GetMailService(string service)
        {
            Logger.InitLogger();
            Logger.Log.Info("Send Email test started.");

            Driver driver = new Driver(Properties.Settings.Default.Browser);
            switch (service)
            {
                case "Yandex":
                    return new Yandex.Ru.Mail(driver);
                default:
                    return new Yandex.Ru.Mail(driver);
            }
        }
    }
}
