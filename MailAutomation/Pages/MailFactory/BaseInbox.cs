﻿using MailAutomation.Utilities;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace MailAutomation.Pages.Mail
{
    public abstract class BaseInbox : BasePage
    {
        public BaseInbox(Driver d)
            : base(d)
        { }

        public static string errorMessageClassName;
        public static ReadOnlyCollection<IWebElement> toolbar;
        public static ReadOnlyCollection<IWebElement> emailLinks;
        public static string newMailToolBar;
        public static string refreshToolBar;
        public static IWebElement inboxLink;
        public static string exitLinkText;

        public static IWebElement ErrorMessage
        {
            get
            {
                return driver.FindElement(By.ClassName(errorMessageClassName));
            }
        }

        public static bool ErrorMessageExists
        {
            get
            {
                return driver.CheckElementExists(By.ClassName(errorMessageClassName));
            }
        }

        public static IWebElement exitLink
        {
            get
            {
                return driver.FindElement(By.LinkText(exitLinkText));

            }
        }

        public virtual bool CheckThatWeAreInside()
        {

            Logger.Log.Debug("Checking there are no errors.");
            if (ErrorMessageExists)
            {
                string errorMessage = ErrorMessage.Text;
                Logger.Log.Error("Error logging in: " + errorMessage);
                return false;
            }
            else
            {
                Logger.Log.Info("Yep! We're inside!");
                return true;
            }
        }

        public void NewEmail()
        {
            Logger.Log.Info("Creating new email.");
            PressToolbarButton(newMailToolBar);
        }

        public void CheckMail()
        {
            Logger.Log.Info("Checking for new mail");
            PressToolbarButton(refreshToolBar);
        }


        public void ClickInbox()
        {
            try
            {
                Logger.Log.Info("Clicking Inbox button");
                inboxLink.Click();
                Logger.Log.Info("Inbox clicked");

            }
            catch (Exception e)
            {
                Logger.Log.Error("Inbox link not found");
            }

        }

        public static void PressToolbarButton(string buttonName)
        {
            try
            {
                Logger.Log.Info("Looking for '" + buttonName + "' button on toolbar.");
                foreach (IWebElement button in toolbar)
                {
                    Logger.Log.Debug("Current button text: " + button.Text);
                    if (button.Text.Equals(buttonName))
                    {
                        button.Click();
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                if (e.GetType().Equals(typeof(StaleElementReferenceException)))
                { 
                    Logger.Log.Debug("Waiting for DOM to get back to normal state.");
                    PressToolbarButton(buttonName);
                }
                else
                {
                    Logger.Log.Fatal(buttonName + " button not found", e);
                }

            }
        }


        public IWebElement FindEmailByTitle(string title)
        {
            IWebElement emailLink = null;
            try
            {
                Logger.Log.Debug("Searching for email with title: " + title);
                emailLink = driver.FindElement(By.PartialLinkText(title));
            }
            catch (Exception e)
            {
                return null;
            }

            
            return emailLink;
        }

        

        public bool CheckEmailByTitle(string title)
        {
            driver.Refresh();

            IWebElement link = FindEmailByTitle(title);
            try
            {
                Logger.Log.Debug("Checking that there is email with title: " + title);
                if (link.Text.Contains(title))
                {
                    Logger.Log.Info("Email with title found: " + title);
                    return true;
                }
                else
                {
                    Logger.Log.Error("Email with title not found: " + title);
                    return false;
                }
            }
            catch (Exception e)
            {
                //TODO: Carefully review logging levels everywhere
                Logger.Log.Fatal("Couldn't find email email with title: " + title, e);
                return false;
            }
        }

        public void ClickEmailByTitle(string title)
        {
            try
            {
                Logger.Log.Debug("Clicking on email with title: " + title);
                IWebElement link = FindEmailByTitle(title);
                link.Click();
            }
            catch (Exception e)
            {
                Logger.Log.Fatal("Couldn't click email link with title: " + title, e);
            }
        }



        internal void Exit()
        {
            exitLink.Click();
            driver.Quit();
        }
    }
}
