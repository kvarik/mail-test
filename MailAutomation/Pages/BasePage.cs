﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MailAutomation.Utilities;
using OpenQA.Selenium;

namespace MailAutomation.Pages
{
    public abstract class BasePage
    {

        //TODO: Check if can be removed
        public abstract string Url
        {
            get;
        }

        public static Driver driver;

        public BasePage(Driver d)
        {
            driver = d;
            if (driver.Url == null ||
               driver.Url.Contains("localhost") ||
               driver.Url.Contains("about:blank") ||
                driver.Url.Contains("data:,")
                )
            {
                driver.Url = Url; 
            }
        }

        public void Navigate(){
            try
            {
                Logger.Log.Debug("Opening page:" + Url);
                driver.Navigate();
            }
            catch (Exception e) {
                Logger.Log.Error("Could not open page: ", e);
            }
        }
    }
}
