﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Safari;
using System.IO;
using System.Collections.ObjectModel;


namespace MailAutomation.Utilities
{
    public class Driver : IWebDriver
    {

        private string browser;
        private IWebDriver driver;
        private string p;

        public string Browser
        {
            get { return browser; }
        }

        
        public Driver(string browser)
        {
            StartBrowser(browser);
        }

        public Driver(string browser, string url)
        {
            try
            {
                //TODO: Driver assignement
                //TODO: driver = DriverFactory.GetDriver(browser)
                StartBrowser(browser);
                this.browser = browser;
                driver.Url = url;
                Logger.Log.Info("Started browser: " + browser);
            } 
            catch (Exception e)
            {
                Logger.Log.Fatal(e.Message, e);
            }
        }


        //TODO : Move to DriverFactory 
        private void StartBrowser(string browser)
        {
            try
            {
                Logger.Log.Debug("Starting browser: " + browser);
                switch (browser)
                {
                    case "Firefox":
                        StartFirefox();
                        break;
                    case "Safari":
                        StartSafari();
                        break;
                    case "IE":
                        StartIE();
                        break;
                    case "Chrome":
                        StartChrome();
                        break;
                    default:
                        StartChrome();
                        break;

                }
                this.browser = browser;
            } 
            catch (Exception e)
            {
                Logger.Log.Fatal("Couldn't start browser: " + browser, e);
            }
        }

        private void StartIE()
        {

            var internetExplorerOptions = new InternetExplorerOptions
            {
                IntroduceInstabilityByIgnoringProtectedModeSettings = true,
                InitialBrowserUrl = "about:blank",
                EnableNativeEvents = true
            };

            driver = new InternetExplorerDriver(Directory.GetCurrentDirectory(), internetExplorerOptions);
        }

        private void StartChrome()
        {
            var chromeOptions = new ChromeOptions();
            var defaultDataFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\..\Local\Google\Chrome\User Data\Default";

            driver = new ChromeDriver(Directory.GetCurrentDirectory(), chromeOptions);
        }

        private void StartSafari()
        {
            SafariOptions options = new SafariOptions();
            options.AddAdditionalCapability("cleanSession", true);
            
            driver = new SafariDriver(options);
        }

        private void StartFirefox()
        {
            FirefoxProfile firefoxProfile = new FirefoxProfile
            {
                AcceptUntrustedCertificates = true,
                EnableNativeEvents = true
            };

            driver = new FirefoxDriver(firefoxProfile);
        }

        /// //////////////////////////////////////////////////////////
        
        public void Navigate()
        {
            Logger.Log.Info("Navigating to: " + Url);
            driver.Navigate();
        }

        public void Close()
        {
            try
            {
                Logger.Log.Debug("Closing browser: " + browser);
                driver.Close();
            }
            catch (Exception e)
            {
                Logger.Log.Fatal("Couldnt close browser: " + browser);
            }
        }

        public string CurrentWindowHandle
        {
            get { return driver.CurrentWindowHandle; }
        }

        public IOptions Manage()
        {
            //TODO: Logging
            return driver.Manage();
        }

        INavigation IWebDriver.Navigate()
        {
            //TODO: Logging
            return driver.Navigate();
        }

        public void Refresh()
        {
            Logger.Log.Info("Refreshing page: " + Url);
            driver.Navigate().Refresh();
        }

        public string PageSource
        {
            get { return driver.PageSource; }
        }

        public void Quit()
        {
            Logger.Log.Info("Exiting browser: " + browser);
            driver.Quit();
        }

        public ITargetLocator SwitchTo()
        {
            //TODO: Logging
            return driver.SwitchTo();
        }

        public string Title
        {
            get { return driver.Title; }
        }

        public string Url
        {
            get
            {
                try
                {
                    return driver.Url;
                }
                catch (Exception e)
                {
                    return "";
                }
            }
            set
            {
                driver.Url = value;
            }
        }

        public System.Collections.ObjectModel.ReadOnlyCollection<string> WindowHandles
        {
            get { return driver.WindowHandles; }
        }

        public IWebElement FindElement(By by)
        {
            //TODO: Replace ifs with override of SafariDriver class
            // Safari does not wait for page to load fully and starts doing other
            // tasks before page is downloaded completely. 
            // To avoid this we manually force some delay and wait till page loaded. 
            if(browser.Equals("Safari"))
            { 
                return WaitForElement(by, 15); 
            }
            try
            {
                Logger.Log.Debug("Searching for element: " + by.ToString());
                return driver.FindElement(by);
                Logger.Log.Debug("Element found:" + by.ToString());
            }
            catch (Exception e)
            {
                Logger.Log.Error("Element not found: ", e);
                return null;
            }
        }

        public bool CheckElementExists(By by)
        {
            try
            {
                Logger.Log.Debug("Checking if element exists: " + by.ToString());
                IWebElement element = driver.FindElement(by);
                Logger.Log.Debug("Element exists: " + by.ToString());
                return true;
            }
            catch (Exception e)
            {
                Logger.Log.Debug("Element doesn't exist: " + by.ToString());
                return false;
            }
        }


        public IWebElement WaitForElement(By by, int seconds)
        {
            IWebElement element = null;

            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(seconds));
            try
            {
                Logger.Log.Debug("Waiting for element for " + seconds + "seconds: " + by.ToString());
                element = driver.FindElement(by);
            }
            catch (Exception e)
            {
                Logger.Log.Error("After " + seconds + " seconds, element not found.", e);
            }

            return element;
            
        }

        public ReadOnlyCollection<IWebElement> WaitForElements(By by, int seconds)
        {

            ReadOnlyCollection<IWebElement> elements = null;
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(seconds));
            try
            {
                Logger.Log.Debug("Waiting for elements for " + seconds + " seconds: " + by.ToString());
                elements = driver.FindElements(by);
            }
            catch (Exception e)
            {
                Logger.Log.Error("After " + seconds + "seconds, elements are not found.", e);
            }

            return elements;
        }


        public bool CheckCouldNotFindElement(By by)
        {
            try
            {
                Logger.Log.Debug("Checking if there's no element: " + by.ToString());
                driver.FindElement(by);
                Logger.Log.Error("Found element: " + by.ToString());
                return false;
            }
            catch (NoSuchElementException e)
            {
                Logger.Log.Debug("All ok, not found elemet: " + by.ToString());
                return true; 
            }
        }

        public System.Collections.ObjectModel.ReadOnlyCollection<IWebElement> FindElements(By by)
        {
            //TODO: This goes to safari wrapper 
            // Safari doesn't like to wait
            if (browser.Equals("Safari") || browser.Equals("IE")) return WaitForElements(by, 15);
            return driver.FindElements(by);
        }

        public void Dispose()
        {
            driver.Dispose();
        }
    }
}
